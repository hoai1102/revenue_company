import axios from 'axios';
import Swal from 'sweetalert2';

const getAllClient = () => {
    return axios
        .get(
            `https://3ou6mgyoaa.execute-api.ap-northeast-1.amazonaws.com/dev/getAll`
        )
        .then((data) => {
            if (data.data) {
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

const getAllPerson = () => {
    return axios
        .get(
            `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/getAllPerson`
        )
        .then((data) => {
            if (data.data) {
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

const addPerson = (input) => {
    return axios
        .post(
            `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/addPerson`,
            {
                ...input,
                Code: Number(input.Code),
                ClientCode: Number(input.ClientCode),
                Tel: Number(input.Tel),
            }
        )
        .then((data) => {
            if (data.data) {
                Swal.fire(`Create success!`, '', 'success');
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

const deletePerson = (id) => {
    return axios
        .delete(
            `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/deletePerson`,
            { data: { id: id } }
        )
        .then((data) => {
            if (data.data) {
                Swal.fire(`Delete Success`, '', 'success');
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

const updatePerson = (id, data) => {
    return axios
        .put(
            `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/updatePerson`,
            {
                id: id,
                ...data,
                Code: Number(data.Code),
                ClientCode: Number(data.ClientCode),
                Tel: Number(data.Tel),
            }
        )
        .then((data) => {
            if (data.data) {
                Swal.fire(`Update Success`, '', 'success');
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

export const PersonApis = {
    getAllClient,
    getAllPerson,
    addPerson,
    deletePerson,
    updatePerson,
};
