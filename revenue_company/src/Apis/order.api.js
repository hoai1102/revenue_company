import axios from 'axios';
import Swal from 'sweetalert2';
const axiosClient = axios.create({
  baseURL: 'https://3ou6mgyoaa.execute-api.ap-northeast-1.amazonaws.com/dev',
  // baseURL: 'http://localhost:5001/hotel-management-se/us-central1/api',
  headers: {
    'Content-Type': 'application/json',
  },
});

const getAllOrders = () => {
  return axiosClient
    .get(`/getAllOrders`)
    .then((data) => {
      if (data.data) {
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};
const addOrder = (device) => {
  return axiosClient
    .post(`/addOrder`, {
      // name: client.name,
      // code: client.code,
      // postcode: Number(client.postcode),
      // address: client.address,
      // rank: client.rank,
      ...device,
      size: +device.size,
      price: +device.price,
    })
    .then((data) => {
      if (data.data) {
        Swal.fire(`Create Success`, '', 'success');
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};
const deleteOrder = (id) => {
  return axiosClient
    .delete(`/deleteOrder`, {
      params: {
        id: id,
      },
    })
    .then((data) => {
      if (data.data) {
        Swal.fire(`Delete Success`, '', 'success');
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};
const updateOrder = (id, data) => {
  return axiosClient
    .put(`/updateOrder`, {
      id: id,
      ...data,
      size: +data.size,
      price: +data.price,
    })
    .then((data) => {
      if (data.data) {
        Swal.fire(`Update Success`, '', 'success');
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};
export const orderApis = {
  getAllOrders,
  addOrder,
  deleteOrder,
  updateOrder,
};
