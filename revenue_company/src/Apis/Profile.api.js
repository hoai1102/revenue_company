import axios from "axios";
import Swal from "sweetalert2";

const getAllProfile = () => {
  return axios
    .get(
      `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/getAllProfile`
    )
    .then((data) => {
      if (data.data) {
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};

const getOneProfile = (Username) => {
  return axios
    .get(
      `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/getOneProfile`,
      // { data: { Username: Username } }
      { params: { Username: Username } }
    )
    .then((data) => {
      if (data.data) {
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};

const addProfile = (profile) => {
  // if (profile.Password === profile.ConfirmPassword && profile.Password !== "")
  return axios
    .post(
      `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/addProfile`,
      {
        ...profile,
        Phone: Number(profile.Phone),
      }
    )
    .then((data) => {
      if (data.data) {
        Swal.fire(
          `Create success!\nYou can Log in with new account`,
          "",
          "success"
        );
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};

const deleteProfile = (Username) => {
  return axios
    .delete(
      `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/deleteProfile`,
      { data: { Username: Username } }
    )
    .then((data) => {
      if (data.data) {
        Swal.fire(`Delete Success`, "", "success");
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};

const updateProfile = (Username, data) => {
  return axios
    .put(
      `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/updateProfile`,
      {
        Username: Username,
        ...data,
        Phone: Number(data.Phone),
      }
    )
    .then((data) => {
      if (data.data) {
        Swal.fire(`Update Success`, "", "success");
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};

export const ProfileApis = {
  getAllProfile,
  getOneProfile,
  addProfile,
  deleteProfile,
  updateProfile,
};
