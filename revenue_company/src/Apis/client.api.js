import axios from 'axios';
import Swal from 'sweetalert2';
const axiosClient = axios.create({
  baseURL: 'https://3ou6mgyoaa.execute-api.ap-northeast-1.amazonaws.com/dev',
  // baseURL: 'http://localhost:5001/hotel-management-se/us-central1/api',
  headers: {
    'Content-Type': 'application/json',
  },
});

const getAllClients = () => {
  return axiosClient
    .get(`/getAll`)
    .then((data) => {
      if (data.data) {
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};
const addClient = (client) => {
  return axiosClient
    .post(`/addUser`, {
      // name: client.name,
      // code: client.code,
      // postcode: Number(client.postcode),
      // address: client.address,
      // rank: client.rank,
      ...client,
      postcode: Number(client.postcode),
    })
    .then((data) => {
      if (data.data) {
        Swal.fire(`Create Success`, '', 'success');
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};
const deleteClient = (id) => {
  return axiosClient
    .delete(`/delete`, {
      params: {
        id: id,
      },
    })
    .then((data) => {
      if (data.data) {
        Swal.fire(`Delete Success`, '', 'success');
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};
const updateClient = (id, data) => {
  return axiosClient
    .put(`/update`, {
      id: id,
      ...data,
      postcode: Number(data.postcode),
    })
    .then((data) => {
      if (data.data) {
        Swal.fire(`Update Success`, '', 'success');
        return data.data;
      }
    })
    .catch((error) => console.log(error));
};
export const clientApis = {
  getAllClients,
  addClient,
  deleteClient,
  updateClient,
};
