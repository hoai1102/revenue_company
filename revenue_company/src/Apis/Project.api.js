import axios from 'axios';
import Swal from 'sweetalert2';

const getAllClient = () => {
    return axios
        .get(
            `https://3ou6mgyoaa.execute-api.ap-northeast-1.amazonaws.com/dev/getAll`
        )
        .then((data) => {
            if (data.data) {
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

const getAllPerson = () => {
    return axios
        .get(
            `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/getAllPerson`
        )
        .then((data) => {
            if (data.data) {
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

const getAllProject = () => {
    return axios
        .get(
            `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/getAllProject`
        )
        .then((data) => {
            if (data.data) {
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

const addProject = (input) => {
    return axios
        .post(
            `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/addProject`,
            {
                ...input,
                Id: Number(input.Id)
            }
        )
        .then((data) => {
            if (data.data) {
                Swal.fire(`Create success!`, '', 'success');
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

const deleteProject = (id) => {
    return axios
        .delete(
            `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/deleteProject`,
            { data: { id: id } }
        )
        .then((data) => {
            if (data.data) {
                Swal.fire(`Delete Success`, '', 'success');
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

const updateProject = (id, data) => {
    return axios
        .put(
            `https://yodaebat2a.execute-api.ap-northeast-1.amazonaws.com/dev/updateProject`,
            {
                id: id,
                ...data,
                Id: Number(data.Id),
            }
        )
        .then((data) => {
            if (data.data) {
                Swal.fire(`Update Success`, '', 'success');
                return data.data;
            }
        })
        .catch((error) => console.log(error));
};

export const ProjectApis = {
    getAllClient,
    getAllPerson,
    getAllProject,
    addProject,
    deleteProject,
    updateProject
};
