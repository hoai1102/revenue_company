import Vue from 'vue';
import './plugins/bootstrap-vue';
import App from './App.vue';
import router from './components/Router/router.js';

import store from './components/Store/store.js';
import excel from 'vue-excel-export'
 
Vue.use(excel)
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
