import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import SignIn from '../SignIn/SignIn.vue';
import Home from '../Home.vue';
import Client from '../Client/Client.vue';
import Person from '../Person/Person.vue';
import Project from '../Project/Project.vue';
import Context from '../Context/Context.vue';
import Profile from '../Profile/Profile.vue';
import Purchase from '../Purchase/Purchase.vue';

import store from '../Store/store.js';

const routes = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: { requiresAuth: true },
    children: [
      {
        path: '/home',
        name: 'Home',
        components: {
          default: Context,
        },
      },
      {
        path: '/client',
        name: 'Client',
        components: { default: Client },
      },
      {
        path: '/person',
        name: 'Person',
        components: { default: Person },
      },
      {
        path: '/project',
        name: 'Project',
        components: { default: Project },
      },
      {
        path: '/profile',
        name: 'Profile',
        components: { default: Profile },
      },
      {
        path: '/purchase',
        name: 'Purchase',
        components: { default: Purchase },
      },
    ],
  },
  {
    path: '/signin',
    name: 'SignIn',
    component: SignIn,
    meta: { requiresAuth: false },
  },
];
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});
router.beforeEach((to, from, next) => {
  store.state.user = localStorage['token'];
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!store.state.user) {
      next('/signin');
      console.log(store.state.user, 'hoai');
    } else {
      console.log(store.state.user, 'dsadsada');
      next();
    }
  } else {
    next();
  }
});

export default router;
